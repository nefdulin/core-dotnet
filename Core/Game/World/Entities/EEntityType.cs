namespace QuantumCore.Game.World.Entities
{
    public enum EEntityType
    {
        Monster = 0,
        Npc = 1,
        Player = 6
    }
}